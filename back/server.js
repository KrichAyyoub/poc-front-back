const express =require('express');
const { exec } = require("child_process");

const app = express();
const PORT = 5000;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/generatePlot/:id',(req,res)=>{
    const id = req.params;
    console.log(id)
    const command = `python batch.py ${id}`;
    
    exec(command , (error, stdout, stderr) => {
        if (error) {
            res.json({message: `error: ${error.message}`})
            return;
        }
        if (stderr) {
            res.json({message: `stderr: ${stderr}`})
            return;
        }
        res.json({message: `stdout: ${stdout}`})
    });
})

app.listen(PORT ,()=>console.log(`Connected to ${PORT}`))