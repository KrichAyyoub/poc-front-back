import { Component } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { PlotServiceService, truc } from './services/plot-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poc';
  selectedId;
  response : truc;

  constructor(private plotService : PlotServiceService) {}

  idsList = ['00011_G604', '00012_G604' , '00013_G604'];

  // idChange(event : MatSelectChange){
  //   console.log('id value ', event.value);
  // }

  generateFile(){
    this.plotService.generatePlot(this.selectedId).subscribe(res => {
      console.log(res)
        this.response = res;
    });
  }


}
